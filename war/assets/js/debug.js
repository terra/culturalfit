var debugSyntax;
var debugScript;
var totalSize;
var validation;
var update;
var finishRun = "";
var interval;
var barChart;
var lineCount = 0;
function runDebugger(script,valid,up,speed,tSize){
	update = up;
	debugScript = script;
	validation = valid;
	debugSyntax = esprima.parse(script,{"loc":true});
	
	chartSetup();
	statePosition = ["body"]
	state = ["Program"]
	programPosition = 0;
	interval = speed;
	lineCount = 0;
	totalSize = tSize+1;
	setTimeout(runCurrentLine,100);
}

var statePosition = ["body"]
var state = ["Program"]
var programPosition = 0;
function runCurrentLine(){
	var toExecute = "";
	var toExecuteLineNumber = 0;
	currentSyntax = debugSyntax;
	for( x in statePosition){
		currentSyntax=currentSyntax[statePosition[x]];
	}
		
	if(state == "Program"){
		if(currentSyntax.length <= programPosition){
			
			showCurrentLine(5000);
			finishSuccessfulRun(validation,finishRun);
			return;
		}
		currentSyntax=currentSyntax[programPosition]
		programPosition++;
		if(currentSyntax.type =="ForStatement"){
			statePosition.push("body");
			statePosition.push("body");
		} else {
			toExecute = currentSyntax;
		}
	}
	console.log(escodegen.generate(toExecute));
	console.log(toExecute.loc.start.line);
	eval(escodegen.generate(toExecute));
	showCurrentLine(toExecute.loc.start.line);
	eval(update);
	//get pre element
	// generate appropriate white space
	//write currently executing line
	// execute line
	// on finish restore code

	setTimeout(runCurrentLine,interval);
}

function showCurrentLine(lineNumber){
	var element = document.getElementById('code');
	if(element === null) return;
	var script = "";
	var sLines = debugScript.split("\n"); 
	for(var a = 0; a < totalSize; a++){
		if(a+1 == lineNumber)
			script += "<b class=debugColor>"+htmlEncode(sLines[a])+"</b>";
		else
			script += htmlEncode(sLines[a]);
		script += "\n";
	}
	playTick();
	element.innerHTML=script;  	
}

function finishSuccessfulRun(script,finish){
	try {
		eval(script);
		
	} catch (err) {			
		window.document.getElementById("output").innerHTML = err.toString()+"\n"+script;		
	}
	try {		
		eval(finish);
	} catch (err) {			
		window.document.getElementById("output").innerHTML = err.toString()+"\n"+finish;
		
	}
	//var element = document.getElementById('scoringOutput');
	//$(element).text(script);
	
}

function htmlEncode(str) {
	if(str === undefined) return "";
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function chartSetup(){
	
	var data = {
	    labels: [],
	    datasets: [
	        {
	            label: "A",
	            fillColor: "#F7464A",
	            //strokeColor: "rgba(220,220,220,0.8)",
	            //highlightFill: "rgba(220,220,220,0.75)",
	            //highlightStroke: "rgba(220,220,220,1)",
	            data: []
	        
	        }
	    ]
	};
	var ctx = $("#chart").get(0).getContext("2d");
	barChart = new Chart(ctx).Bar(data);
	
	
}
function levelSuccess(flag,message){
	if(flag)
		$("#next").text("Next")
	else{
		$("#next").text("Retry")
		alert(message);
	}
	finalizeLevel(flag);	
}
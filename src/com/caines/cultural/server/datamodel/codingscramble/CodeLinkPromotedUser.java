package com.caines.cultural.server.datamodel.codingscramble;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.caines.cultural.server.SDao;
import com.caines.cultural.shared.datamodel.GUser;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.google.appengine.api.users.User;
import com.google.common.annotations.GwtIncompatible;
import com.google.gwt.user.client.rpc.GwtTransient;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class CodeLinkPromotedUser implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CodeLinkPromotedUser() {
		// TODO Auto-generated constructor stub
	}
	public CodeLinkPromotedUser(GUser user2, CodeTag ct) {
		user = user2.getRef();
		Current = Ref.create(ct.codePromotedFirst);
		tag = Ref.create(ct);
	}

	@Index
	public Ref<CodeTag> tag;
	@Index
	public Ref<GUser> user;
	
	public Ref<CodeLinkPromoted> Current;
	
	
	@Id
	public Long id;

}

package com.caines.cultural.server.datamodel.codingscramble;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class CodeTag implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CodeTag() {
		// TODO Auto-generated constructor stub
	}

	public CodeTag(String t) {
		tag = t;
		main = !tag.contains("/");
	
	}

	@Id
	public String tag;

	@Index
	boolean main;
	
	
	public List<Ref<CodeContainer>> codeContainerList = new ArrayList<>();

	public CodeLinkPromoted codePromotedFirst;

}

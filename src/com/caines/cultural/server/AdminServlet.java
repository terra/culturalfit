package com.caines.cultural.server;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.caines.cultural.server.datamodel.codingscramble.CodeLink;
import com.caines.cultural.server.datamodel.codingscramble.CodePointer;
import com.caines.cultural.server.datamodel.codingscramble.CodeTag;
import com.caines.cultural.server.datautil.CodeContainerUtil;
import com.caines.cultural.server.datautil.CodeLinkContainerUtil;
import com.caines.cultural.server.datautil.CodeLinkUtil;
import com.caines.cultural.server.datautil.CodeTagUtil;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainerFile;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.RetryOptions;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.googlecode.objectify.Ref;

public class AdminServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String url = req.getParameter("url");
		String[] tag = req.getParameterValues("tag");

		if (url != null) {
			addCodeContainer(url, tag);
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String sameFile = req.getParameter("sameFile");
		if(sameFile != null){
			for(CodeLink l : SDao.getCodeLinkDao().listByProperty("sameFile", false)){
				System.out.println(l.cp1.get().line);
			}
			return;
		}
		String delete = req.getParameter("delete");
		if (delete != null) {
			SDao.getCodeLinkDao().deleteAll(
					SDao.getCodeLinkDao().getQuery().list());
			SDao.getCodeLinkPromotedDao().deleteAll(
					SDao.getCodeLinkPromotedDao().getQuery().list());
			SDao.getCodeLinkPromotedUserDao().deleteAll(
					SDao.getCodeLinkPromotedUserDao().getQuery().list());
			SDao.getCodeContainerDao().deleteAll(
					SDao.getCodeContainerDao().getQuery().list());
			
			SDao.getCodeTagDao().deleteAll(SDao.getCodeTagDao().getQuery().list());
			
		}
			Queue queue = QueueFactory.getQueue("addFiles");
			
			String u = "https://raw.githubusercontent.com/apache/hadoop-common/trunk/hadoop-mapreduce-project/hadoop-mapreduce-examples/src/main/java/org/apache/hadoop/examples/AggregateWordCount.java";
			String tag = "java";
			addQueue(queue, u, tag);

			u="https://raw.githubusercontent.com/python/cpython/c7688b44387d116522ff53c0927169db45969f0e/Lib/json/decoder.py";
			addQueue(queue, u, "python");
			u="https://raw.githubusercontent.com/python/cpython/c7688b44387d116522ff53c0927169db45969f0e/Lib/json/scanner.py";
			addQueue(queue, u, "python");
			
			u="https://raw.githubusercontent.com/fogleman/Minecraft/master/main.py";
			addQueue(queue, u, "python");
			
			

		// https://raw.githubusercontent.com/NimbusFoundry/Foundry/gh-pages/app/app.js
		// https://raw.githubusercontent.com/nathanmarz/storm-starter/master/src/jvm/storm/starter/WordCountTopology.java

		// addCodeContainer("https://github.com/wesnoth/wesnoth/blob/master/src/editor/editor_display.cpp",
		// new String[]{"C++/wesnoth","games"});

		// addCodeContainer("", new String[]{""});
		// addCodeContainer("", new String[]{""});
		// addCodeContainer("", new String[]{""});
		// addCodeContainer("", new String[]{""});

		// addCodeContainer("https://raw.githubusercontent.com/apache/hadoop-common/trunk/hadoop-mapreduce-project/hadoop-mapreduce-examples/src/main/java/org/apache/hadoop/examples/AggregateWordCount.java",
		// new String[]{"java/hadoop/aggregateCount$"});
		// addCodeContainer("https://raw.githubusercontent.com/apache/mahout/master/examples/src/main/java/org/apache/mahout/cf/taste/example/bookcrossing/BookCrossingDataModel.java",
		// new String[]{"java/hadoop/mahout$"});
		// addCodeContainer("https://raw.githubusercontent.com/jquery/jquery/master/src/core.js",
		// new String[]{"javascript/jquery/jquery$"});
		// addCodeContainer("https://raw.githubusercontent.com/google/flatbuffers/master/src/flatc.cpp",
		// new String[]{"C++/serialization/flatbuffers$"});
		// addCodeContainer("https://raw.githubusercontent.com/torvalds/linux/master/kernel/locking/locktorture.c",
		// new String[]{"C/linux/kernel$"});
		// addCodeContainer("https://raw.githubusercontent.com/ehsan/ogre/master/OgreMain/src/OgreAnimation.cpp",
		// new String[]{"C++/ogre3d$"});
		// addCodeContainer("https://raw.githubusercontent.com/nebez/floppybird/gh-pages/js/main.js",
		// new String[]{"javascript/flappybird$","games"});

	}

	public void addQueue(Queue queue, String u, String... tag) {
		
		TaskOptions param = TaskOptions.Builder
				.withUrl("/culturalfit/admin")
				.param("url",
						u).retryOptions(
						RetryOptions.Builder.withTaskRetryLimit(0)
								.taskAgeLimitSeconds(10000)).countdownMillis(1000);
		for(String a : tag){
			param = param.param("tag", a);
		}
		
		queue.add(param);		
				
	}

	public static void setupCrossCodeLink(Ref<CodePointer> cp1, Ref<CodePointer> cp2) {
		if(cp2.get().container == null||cp1.get().container.equals(cp2.get().container)){
			return;
		}
	
		CodeLinkUtil.getCodeLink(cp1, cp2);
	}

	public static void addCodeContainer(String url, String[] tagsA) {
		CodeContainerFile ccf = CodeContainerUtil.setup(url,
				Arrays.asList(tagsA));
		if(ccf == null){
			return;
		}
		CodeContainer c = CodeContainerUtil.setupLinks(ccf);
		for (String tag : ccf.tags) {
			if (!tag.endsWith("$")) {
				//continue;
			}
			CodeTag ct = CodeTagUtil.getCodeTag(tag);
			Map<String, List<CodeLink>> m = new HashMap<>();
			for (CodeLink cl : SDao.getCodeLinkDao().getQuery()
					.filter("codeTag", ct).list()) {
				if (!m.containsKey(cl.cp1.get().line)) {
					m.put(cl.cp1.get().line, new ArrayList<CodeLink>());
				}
				m.get(cl.cp1.get().line).add(cl);
			}
			for (List<CodeLink> clL : m.values()) {
				for (CodeLink cl1 : clL) {
					for (CodeLink cl2 : clL) {
						setupCrossCodeLink(cl1.cp1, cl2.cp1);
						setupCrossCodeLink(cl1.cp1, cl2.cp2);
						setupCrossCodeLink(cl1.cp2, cl2.cp1);
						setupCrossCodeLink(cl1.cp2, cl2.cp2);

					}

				}

			}

		}
		CodeLinkContainerUtil.generateCodeLinkContainer(SDao.getRef(c));
	}
}

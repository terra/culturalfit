package com.caines.cultural.server.datautil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import com.caines.cultural.server.SDao;
import com.caines.cultural.server.datamodel.codingscramble.CodeLink;
import com.caines.cultural.server.datamodel.codingscramble.CodePointer;
import com.caines.cultural.server.datamodel.codingscramble.CodeTag;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.google.common.annotations.GwtIncompatible;
import com.googlecode.objectify.Ref;

public class CodeLinkUtil {

	public static CodeLink getCodeLink(Ref<CodePointer> cp1, Ref<CodePointer> cp2) {
		if(cp1 == null||cp1.get().equals(cp2.get())){
			return null;
		}
		CodeLink cl=SDao.getCodeLinkDao().get(""+cp1.getKey().getId()+cp2.getKey().getId());
		if(cl == null){
			cl = new CodeLink();
			cl.cp1 = cp1;
			cl.cp2 = cp2;
			cl.id = ""+cp1.getKey().getId()+cp2.getKey().getId();
			Ref<CodeContainer> container = cp1.get().container!= null?cp1.get().container:cp2.get().container;
			for(String a : container.get().cf.get().tags){
				//if(a.endsWith("$")){
					cl.codeTag = SDao.getRef(CodeTagUtil.getCodeTag(a));
				//}
			} 
			cl.codeContainer = container;
			cl.codeContainer2 = cp2.get().container;
			cl.sameFile = cl.codeContainer.equals(cl.codeContainer2);
			cl.linked++;
			SDao.getCodeLinkDao().put(cl);
		}
		return cl;
	}
}

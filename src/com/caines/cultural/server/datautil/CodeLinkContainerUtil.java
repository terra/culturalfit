package com.caines.cultural.server.datautil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.caines.cultural.server.SDao;
import com.caines.cultural.server.datamodel.codingscramble.CodeLink;
import com.caines.cultural.server.datamodel.codingscramble.CodePointer;
import com.caines.cultural.shared.datamodel.GUser;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeLinkContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeLinkContainer.CodeLinkContainerData;
import com.google.common.annotations.GwtIncompatible;
import com.googlecode.objectify.Ref;

public class CodeLinkContainerUtil {
	
	public static CodeLinkContainer generateCodeLinkContainer(Ref<CodeContainer> c) {
		// 
		Map<String,CodeLinkContainerData> cMap = new HashMap<>();
		CodeLinkContainer clr = new CodeLinkContainer();
		List<Ref<CodeContainer>> alreadyContains = new ArrayList<>();
		for(CodeLink cl :SDao.getCodeLinkDao().getQuery().filter("codeContainer", c).list()){
			
			
			CodePointer cp1 = cl.cp1.get();
			CodeLinkContainerData clc = cMap.get(cp1.line);
			if(clc == null){
				clc = new CodeLinkContainerData();
				//clc.codeContainer = cl.codeContainer;
				cMap.put(cp1.line, clc);
				clc.name = cp1.line;
			}
			if(!cl.codeContainer.equals(cl.codeContainer2)){								
				if(cl.codeContainer2 != null){
					int index = alreadyContains.indexOf(cl.codeContainer2);
					if(index == -1){
						alreadyContains.add(cl.codeContainer2);
						String url = cl.codeContainer2.get().cf.get().url;
						
						clr.linkedContainerNames.add(url);
						clr.linkedContainerIds.add(""+cl.codeContainer2.getKey().getId());
						List<String> l = new ArrayList<>();
						l.add(cp1.line);
						clr.linkedKeyWords.add(l);
					} else {
						List<String> list = clr.linkedKeyWords.get(index);
						if(!list.contains(cp1.line))
							list.add(cp1.line);
					}
				}
				
				//clc.crossLinkContainer.add(cl.codeContainer2);
				
				continue;
			}
			if(cl.notLinked > cl.linked+ cl.highlyLinked){
				addCL(cl, clc.notLinkedPointers);
			} else if (cl.linked > cl.highlyLinked){				
				addCL(cl, clc.linkedPointers);
			} else {
				addCL(cl, clc.highlyLinkedPointers);
			}
		}
		
		
		clr.clcList = new ArrayList(cMap.values());
		return clr;
	}

	public static void addCL(CodeLink cl, List<Integer> linkedPointers) {
		if(!linkedPointers.contains(cl.cp1.get().lineNumber))
			linkedPointers.add(cl.cp1.get().lineNumber);
		if(!linkedPointers.contains(cl.cp2.get().lineNumber))
			linkedPointers.add(cl.cp2.get().lineNumber);
		
	}
}

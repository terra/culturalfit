package com.caines.cultural.server.datautil;

import java.util.HashMap;
import java.util.List;

import com.caines.cultural.server.SDao;
import com.caines.cultural.shared.LoginInfo;
import com.caines.cultural.shared.datamodel.codingscramble.CodeUserDetails;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;

public class CodeUserDetailsUtil {
	public static void increment(CodeUserDetails cud,List<String> tags) {
		if(cud == null||tags == null){
			return;
		}
		for(String s : tags){
			
			Integer i =cud.countMap.get(s);
			if(i == null) i = 0;
			i++;
			cud.countMap.put(s, i);
		}
		SDao.getCodeUserDetailsDao().put(cud);
		
	}
	
	public static void incrementPromoted(CodeUserDetails cud,List<String> tags,boolean like) {
		if(cud == null||tags == null){
			return;
		}
		for(String s : tags){
			
			Integer i =cud.promotedCount.get(s);
			if(i == null) i = 0;
			i++;
			cud.promotedCount.put(s, i);
			
			
			
			
			incrementCorrect(cud, like, s);
		}
		
		SDao.getCodeUserDetailsDao().put(cud);
		
	}

	private static void incrementCorrect(CodeUserDetails cud, boolean like,
			String s) {
		Integer i;
		
		i =cud.promotedCorrect.get(s);
		if(i == null) i = 0;			
		if(like)i++;
		cud.promotedCorrect.put(s, i);
	}
	
	
	public static Ref<CodeUserDetails> getRef(Long id) {
		return Ref.create(Key.create(CodeUserDetails.class, id));
	}


	public static CodeUserDetails getByUser(LoginInfo login) {
		CodeUserDetails cud = SDao.getCodeUserDetailsDao().getByProperty("gUser", login.gUser.getRef());
		if(cud != null){
			return cud;
		}
		cud = new CodeUserDetails();
		
		cud.gUser = login.gUser.getRef();
		return cud;
	}

}

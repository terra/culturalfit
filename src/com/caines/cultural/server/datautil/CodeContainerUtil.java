package com.caines.cultural.server.datautil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import com.caines.cultural.server.SDao;
import com.caines.cultural.server.datamodel.codingscramble.CodePointer;
import com.caines.cultural.server.datamodel.codingscramble.CodeTag;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainerFile;
import com.google.common.annotations.GwtIncompatible;
import com.sun.org.apache.xerces.internal.impl.dv.xs.DecimalDV;

public class CodeContainerUtil {

	static List<String> javaList = Arrays.asList(new String[] { "com", "org",
			"public", "class", "static", "private", "final", "new", "String" ,"throws"});
	static List<String> javaScriptList = Arrays.asList(new String[] { "var",
			"slice", "concat", "push", "indexOf", "function", "window",
			"return", "new", "for", "this", "document","if" });
	static List<String> cppList = Arrays
			.asList(new String[] { "include", "h","const","void" });
	static List<String> pyList = Arrays
			.asList(new String[] { "if", "raise","None","return","try","except","elif","for","float","or","and","else","def","string","True","False" ,"self"});

	@GwtIncompatible("")
	public static CodeContainerFile setup(String url, List<String> tagsA) {

		CodeContainerFile cf = SDao.getCodeContainerFileDao().getOrCreate(url,CodeContainerFile.class);
		if(cf.url != null){
			return cf;
		}
		
		cf.url = url;
		List<String> totalTags = new ArrayList<>();
		for (String b : tagsA) {

			String tag = "";
			for (String d : b.split("/")) {
				tag += d + "/";
				totalTags.add(tag.substring(0, tag.length() - 1));
			}
		}
		cf.tags.addAll(totalTags);
		List<String> file = cf.file;
		// ...
		getFile(url, file);
		SDao.getCodeContainerFileDao().put(cf);
		return cf;
		// setupLinks(c, cf);
	}

	public static void getFile(String url, List<String> file) {
		HttpURLConnection o;
		BufferedReader reader;
		try {
			URL urlU = new URL(url);
			o = (HttpURLConnection) urlU.openConnection();
			o.connect();
			if(o.getResponseCode() != 200){
				return;
			}
			
			reader = new BufferedReader(new InputStreamReader(
					urlU.openStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				file.add(line);
			}

			reader.close();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static CodeContainer setupLinks(CodeContainerFile cf) {
		CodeContainer c = new CodeContainer();

		List<String> toIgnore = new ArrayList<>();
		if (cf.url.endsWith(".java")) {
			toIgnore = javaList;
		}
		if (cf.url.endsWith(".js")) {
			toIgnore = javaScriptList;
		}
		if (cf.url.endsWith(".cpp")) {
			toIgnore = cppList;
		}
		if (cf.url.endsWith(".py")) {
			toIgnore = pyList;
		}
		SDao.getCodeContainerDao().put(c);
		for (String t : cf.tags) {
			CodeTag ct = SDao.getCodeTagDao().get(t);
			if (ct == null)
				ct = new CodeTag(t);
			ct.codeContainerList.add(SDao.getRef(c));
			SDao.getCodeTagDao().put(ct);
		}
		HashSet<String> hs = new HashSet<>();
		List<Boolean> bList = new ArrayList<>();
		boolean isComment = false;
		
		for (String line : cf.file) {
			// System.out.println(line);
			// q.add(line);
			String fullLine = line;
			line = line.trim();
			isComment = checkComment(isComment, line);
			bList.add(isComment);
			if (isComment || line.contains("import")
					//|| line.startsWith("*") 
					|| line.length() == 0
					|| line.contains("//")||Character.isUpperCase(line.charAt(0))||line.startsWith("#")) {
				continue;
				// q.remove(line);
			}
			
			char[] cA = new char[line.length()];
			int count = 0;
			Character quote = null;
			for(char ch : line.toCharArray()){
				if(ch == '\"'||ch =='\''){
					if(quote == null){
						quote = ch;	
					} else {
						quote = null;
					}
				}				
				cA[count] = ch;
				if(quote != null){
					cA[count] = '@';	
				}
				count++;		
			}
			line = new String(cA);
			for (String b : line.split("[^A-Za-z0-9_-]")) {
				if (!toIgnore.contains(b)&&!isNumeric(b))
					hs.add(b);
			}

		}

		hs.remove("");
		c.hs = new ArrayList<>(hs);
		c.cf = SDao.getRef(cf);

		// run through h
		
		
		System.out.println(cf.url);
		String nextLink = null;
		
		while (true) {// line2 == -1) {
			int line1 = -1;
			int line2 = -1;
			if (c.hs.size() <= c.nextLink) {
				c.nextLink = 0;
				break;
			}
			nextLink = c.hs.get(c.nextLink);
			
			for (int a = c.nextLine; a < cf.file.size(); a++) {
				
				String b = cf.file.get(a);
				if(bList.get(a)){
					System.out.println(a);
					continue;
				}
				String btrim = b.trim();
				if (btrim.startsWith("import") || btrim.startsWith("package")
						|| b.contains("//")||btrim.startsWith("#")) {
					continue;
				}
				
				for (String m : btrim.split("[^A-Za-z0-9]")) {
					if (m.length() == 0) {
						continue;
					}
					if (m.equals(nextLink)) {
						if (line1 == -1) {
							line1 = a;
							break;
						} else if (line2 == -1) {
							line2 = a;
							c.nextLine = a + 1;
							
							CodeLinkUtil.getCodeLink(CodePointer
									.getCodePointer(c, line1, nextLink).getRef(),
									CodePointer.getCodePointer(c, line2,
											nextLink).getRef());
							break;
						}

					}
				}
				if (line2 != -1) {
					break;
				}
			}

			if (line2 == -1) {
				line1 = -1;
				c.nextLink++;
				c.nextLine = 0;

			}
		}
		SDao.getCodeContainerDao().put(c);
		return c;
	}
	 
	public static boolean checkComment(boolean isComment, String line) {
		if (line.contains("/*")) {
			isComment = true;
		}
		if (line.contains("*/")) {
			isComment = false;
		}

		if (!isComment&&line.contains("\"\"\"")) {
			isComment = true;
		}else if (isComment&&line.contains("\"\"\"")) {
			isComment = false;
		}
		
		return isComment;
	}

	
	public static boolean isNumeric(String str)
	{
	    return str.matches("[+-]?\\d*(\\.\\d+)?");
	}
}

package com.caines.cultural.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import com.caines.cultural.client.BasicScramblerService;
import com.caines.cultural.server.datamodel.codingscramble.CodeLink;
import com.caines.cultural.server.datamodel.codingscramble.CodeLinkPromoted;
import com.caines.cultural.server.datamodel.codingscramble.CodeLinkPromotedUser;
import com.caines.cultural.server.datamodel.codingscramble.CodeTag;
import com.caines.cultural.server.datautil.CodeLinkContainerUtil;
import com.caines.cultural.server.datautil.CodeLinkUtil;
import com.caines.cultural.server.datautil.CodeUserDetailsUtil;
import com.caines.cultural.shared.LoginInfo;
import com.caines.cultural.shared.Tuple;
import com.caines.cultural.shared.UserInfo;
import com.caines.cultural.shared.container.ScrambleProfileTransfer;
import com.caines.cultural.shared.container.ScramblerQuestion;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainerFile;
import com.caines.cultural.shared.datamodel.codingscramble.CodeLinkContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeUserDetails;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Ref;

public class BasicScramblerImpl extends RemoteServiceServlet implements
		BasicScramblerService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private LoginInfo login() {
		return LoginService.login(getThreadLocalRequest(),
				getThreadLocalResponse());
	}

	@Override
	public void addCodePage(String url, String tags) {
		String[] tagsA = tags.split("[,\\s]");
		AdminServlet.addCodeContainer(url, tagsA);
		//CodeContainerUtil.setup(url, Arrays.asList(tagsA));
		
	}

	@Override
	public ScrambleProfileTransfer getProfileContent() {

		CodeUserDetails cud = CodeUserDetailsUtil.getByUser(login());
		ScrambleProfileTransfer spt = new ScrambleProfileTransfer();
		
		spt.promotedCount = cud.promotedCount;
		for(Entry<String,Integer> e : cud.promotedCount.entrySet()){
			spt.promotedCorrectness.put(e.getKey(),((double)cud.promotedCorrect.get(e.getKey())/e.getValue()));
		}
		spt.countSection = cud.countMap;
		return spt;
	}

	@Override
	public UserInfo getUserInfo() {

		LoginInfo login = login();
		if (login == null) {
			return null;
		}

		UserInfo userInfo = new UserInfo();
		// userInfo.isAdmin = login.gUser.isAdmin();
		userInfo.isAdmin = true;
		userInfo.uid = login.gUser.id;
		return userInfo;
	}

	@Override
	public ScramblerQuestion getNextLines(String tag,boolean showPromote) {
		// Pull next from algorithm
		LoginInfo li = login();

		if(tag== null||tag.equals("")){
			tag = "python";
		}
		CodeTag ct = SDao.getCodeTagDao().get(tag);
		CodeContainer c = null;

		
		CodeLink cl = null;
		ScramblerQuestion sq = new ScramblerQuestion();
		if(showPromote&&ct != null&&ct.codePromotedFirst != null){
			CodeLinkPromotedUser clpu=SDao.getCodeLinkPromotedUserDao().getQuery().filter("user", li.gUser.getRef()).filter("tag",Ref.create(ct)).first().now();
			if(clpu == null){				
				clpu = new CodeLinkPromotedUser(li.gUser,ct);
			}
			if(clpu.Current != null){
				cl = clpu.Current.get().codeLink.get();
				clpu.Current = clpu.Current.get().next;
				SDao.getCodeLinkPromotedUserDao().put(clpu);
				sq.promoted = true;
				c = cl.codeContainer.get();	
			}
		}
		if(cl == null){
		
			if (ct != null) {
				c = ct.codeContainerList.get(
						new Random().nextInt(ct.codeContainerList.size())).get();
			} else {
				List<CodeContainer> ccl = SDao.getCodeContainerDao().getQuery()
						.list();
				c = ccl.get(new Random().nextInt(ccl.size()));
			}
			
			if(Math.random() > .5){
				cl =SDao.getRandom(SDao.getCodeLinkDao().getQuery().filter("codeContainer",c).filter("sameFile",false));
			} else {
				cl = SDao.getRandom(SDao.getCodeLinkDao().getQuery().filter("codeContainer",c).filter("sameFile",true));
			}
			if(cl == null){
				cl = SDao.getRandom(SDao.getCodeLinkDao().getQuery().filter("codeContainer",c).filter("sameFile",true));
			}
		
		}
		
		sq.linkedText = cl.cp1.get().line;
		sq.line1 = cl.cp1.get().lineNumber;
		sq.line2 = cl.cp2.get().lineNumber;
		CodeContainerFile cf = c.cf.get();
		sq.rawFile = cf.file;
		sq.rawFile2 = cl.codeContainer2.get().cf.get().file;
	
		sq.tag1 = cf.tags.get(0);
		sq.filename = cf.url.substring(cf.url.lastIndexOf("/"));
		sq.filename2 = cl.codeContainer2.get().cf.get().url.substring(cf.url.lastIndexOf("/"));
		sq.url = "" + c.id;
		li.gUser.cv.cp1 = cl.cp1;
		li.gUser.cv.cp2 = cl.cp2;
		SDao.getGUserDao().put(li.gUser);
		SDao.getCodeContainerDao().put(c);
		return sq;
	}

	@Override
	public void linkCode(String linkType) {
		LoginInfo li = login();

		CodeLink cl = CodeLinkUtil
				.getCodeLink(li.gUser.cv.cp1, li.gUser.cv.cp2);
		
		boolean linked = "linked".equals(linkType);
		if (linked) {
			cl.linked++;
		}
		boolean notLinked = "notLinked".equals(linkType);
		if (notLinked) {
			cl.notLinked++;
		}
		SDao.getCodeLinkDao().put(cl);

		CodeLinkPromoted clp = getCodeLinkPromoted(cl);
		CodeUserDetails cud = CodeUserDetailsUtil.getByUser(li);
		List<String> tags = cl.codeContainer.get().cf.get().tags;
		boolean like = cl.linked >  cl.notLinked&&linked|| cl.linked < cl.notLinked&&notLinked; 
		if(clp != null){
			CodeUserDetailsUtil.incrementPromoted(cud, tags, like);
		}
		
		
		CodeUserDetailsUtil.increment(cud,tags);
		
	}

	@Override
	public Tuple<CodeContainer, CodeLinkContainer> getContainer(
			String associatedUrl) {
		LoginInfo li = login();

		CodeContainer codeContainer = SDao.getCodeContainerDao().get(
				Long.parseLong(associatedUrl));
		codeContainer.ccf = codeContainer.cf.get();
		
				
		return new Tuple<CodeContainer, CodeLinkContainer>(codeContainer,
				CodeLinkContainerUtil.generateCodeLinkContainer(
						SDao.getRef(codeContainer)));
	}

	@Override
	public void promote() {
		LoginInfo li = login();

		CodeLink cl = CodeLinkUtil
				.getCodeLink(li.gUser.cv.cp1, li.gUser.cv.cp2);
		if(cl.codeTag == null){
			return;
		}
		CodeLinkPromoted clp=getCodeLinkPromoted(cl);
		
		if(clp == null){
			clp = new CodeLinkPromoted();
			clp.codeLink = Ref.create(cl);
			SDao.getCodeLinkPromotedDao().put(clp);
		}
		clp.numberPromoted++;
		if(clp.numberPromoted >= promotionThreshold){
			if(!clp.alreadyPromoted){
				clp.alreadyPromoted = true;
				CodeLinkPromoted clpLL=cl.codeTag.get().codePromotedFirst;			
				if(clpLL == null){
					cl.codeTag.get().codePromotedFirst = clp;
									
				} else {
					while(clpLL != null&&clpLL.next != null){					
						clpLL = clpLL.next.get();
					}
					clpLL.next = Ref.create(clp);
					SDao.getCodeLinkPromotedDao().put(clpLL);
				}
				
				SDao.getCodeTagDao().put(cl.codeTag.get());	
			}
						
			
		}
		SDao.getCodeLinkPromotedDao().put(clp);
		//additionally front page has a "show me only promoted/ show me everything" toggle
	}

	private CodeLinkPromoted getCodeLinkPromoted(CodeLink cl) {
		return SDao.getCodeLinkPromotedDao().getByProperty("codeLink", cl);
	}
	static int promotionThreshold = 1;
	
	
}

package com.caines.cultural.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.caines.cultural.client.ui.codingscramble.drop.DropScramblerService;
import com.caines.cultural.client.ui.codingscramble.drop.ScramblerLevel;
import com.caines.cultural.server.datamodel.codingscramble.CodeLink;
import com.caines.cultural.server.datamodel.codingscramble.CodePointer;
import com.caines.cultural.server.datamodel.codingscramble.CodeTag;
import com.caines.cultural.server.datautil.CodeContainerUtil;
import com.caines.cultural.shared.LoginInfo;
import com.caines.cultural.shared.Tuple;
import com.caines.cultural.shared.datamodel.GUser;
import com.caines.cultural.shared.datamodel.UserProfile;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.caines.cultural.shared.datamodel.codingscramble.DropMap;
import com.caines.cultural.shared.datamodel.codingscramble.DropMapLevel;
import com.caines.cultural.shared.datamodel.codingscramble.DropMapLevelContainer;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Ref;

public class DropScramblerImpl extends RemoteServiceServlet implements
		DropScramblerService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Map<String, List<Integer>> orderMap = new HashMap<>();
	{
		// orderMap.put("1/1", Arrays.asList(new Integer[] { 2, 0, 1 }));
	}

	@Override
	public ScramblerLevel getCurrentLevel(String title) {
		return null;

	}

	@Override
	public Tuple<DropMapLevelContainer, UserProfile> getMap(String uid) {

		DropMapLevelContainer dmlc = SDao.getDropMapLevelContainerDao().get(1L);
		if (dmlc == null) {
			{
				dmlc = new DropMapLevelContainer();
				DropMapLevel dml = new DropMapLevel("1");
				dmlc.levels.add(dml);
				dml.maps.add(new DropMap("1", "To 15",500));
				dml.maps.add(new DropMap("2", "To 30",400));
				dml.maps.add(new DropMap("3", "To 30 faster",350));
				dml.maps.add(new DropMap("4", "To 30 so fast",300));
				dml.maps.add(new DropMap("5", "To 30 even faster",200));
				dml.maps.add(new DropMap("6", "To 30 so so fast ",150));

				dml = new DropMapLevel("2");
				dmlc.levels.add(dml);
				dml.maps.add(new DropMap("1", "A long string"));
				dml.maps.add(new DropMap("2", "A short string"));
				dml.maps.add(new DropMap("3", "An empty String"));
				dml.maps.add(new DropMap("4", "A sentence"));
				dml.maps.add(new DropMap("5", "A large sentence"));
				dml.maps.add(new DropMap("6", "A book"));

//				dml = new DropMapLevel("3");
//				dmlc.levels.add(dml);
//				dml.maps.add(new DropMap("1", "Juggling two booleans"));
//				dml.maps.add(new DropMap("2", "Booleans and parentheses"));
//				dml.maps.add(new DropMap("3", "Two booleans and xor"));
//				dml.maps.add(new DropMap("4", "Three booleans"));
//				dml.maps.add(new DropMap("5", "Many booleans"));
//
//				dml = new DropMapLevel("4");
//				dmlc.levels.add(dml);
//				dml.maps.add(new DropMap("1", "If then"));
//				dml.maps.add(new DropMap("2", "If If then"));
//				dml.maps.add(new DropMap("3", "If If then If then then"));
//				dml.maps.add(new DropMap("4", "If then else"));
//				dml.maps.add(new DropMap("5", "Many"));
			}
			// for

			// database

			// user interface
			dmlc.id = 1L;

			for (DropMapLevel dml : dmlc.levels) {
				for(DropMap dm : dml.maps){
					String title = dml.name+"/"+dm.name;
					
					String url = getThreadLocalRequest().getServerName();
					url = "http://" + url + ":"
							+ getThreadLocalRequest().getServerPort()
							+ "/mapStructure/";
					CodeContainerUtil.getFile(url + title + "/level.txt",
							dm.linesToDrop);
					// CodeContainerUtil.getFile(url+"/mapStructure/"+title+"/error.txt",
					// dm.errorCheck);
					CodeContainerUtil.getFile(url + title + "/update.txt",
							dm.debugUpdate);
					CodeContainerUtil.getFile(url + title + "/finish.txt",
							dm.debugFinish);
					CodeContainerUtil.getFile(url + title + "/before.txt",
							dm.debugBefore);
					List<Integer> order = orderMap.get(title);
					if (order != null)
						dm.order = new ArrayList<Integer>(order);
					while (dm.linesToDrop.remove(""))
						;
					
				}
			}

			SDao.getDropMapLevelContainerDao().put(dmlc);

		}

		LoginInfo li;
		UserProfile up;
		if (uid != null) {
			up = SDao.getUserProfileDao().getByProperty("user", Ref.create(GUser.getKey(uid)));
		} else {
			li = login();
			up = SDao.getUserProfileDao().getByProperty("user", li.gUser.getRef());
		}
		if(up == null){
			up = new UserProfile(login().gUser);
			SDao.getUserProfileDao().put(up);
		}

		return new Tuple<>(dmlc, up);
	}

	@Override
	public void updateProfile(UserProfile up) {
		LoginInfo li = login();
		UserProfile oup = SDao.getUserProfileDao().getByProperty("user", li.gUser.getRef());
		oup.currentLevel =up.currentLevel;
		oup.currentMap = up.currentMap;
		oup.maps = up.maps;
		SDao.getUserProfileDao().put(oup);
	}

	private LoginInfo login() {
		return LoginService.login(getThreadLocalRequest(),
				getThreadLocalResponse());
	}

}

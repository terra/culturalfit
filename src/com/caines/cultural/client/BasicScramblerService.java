package com.caines.cultural.client;

import com.caines.cultural.shared.Tuple;
import com.caines.cultural.shared.UserInfo;
import com.caines.cultural.shared.container.ScrambleProfileTransfer;
import com.caines.cultural.shared.container.ScramblerQuestion;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeLinkContainer;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("basic")
public interface BasicScramblerService extends RemoteService {

	ScramblerQuestion getNextLines(String tag,boolean showPromote);

	
	void addCodePage(String url,String tags);
	ScrambleProfileTransfer getProfileContent();

	UserInfo getUserInfo();
	
	Tuple<CodeContainer, CodeLinkContainer> getContainer(String associatedUrl);

	void linkCode(String linkType);


	void promote();
	
	
	
}

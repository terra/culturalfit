package com.caines.cultural.client.ui.codingscramble.drop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.caines.cultural.client.SimpleFront;
import com.caines.cultural.client.ui.callback.FailAlertCallback;
import com.caines.cultural.client.ui.callback.VoidCallback;
import com.caines.cultural.shared.Tuple;
import com.caines.cultural.shared.UserInfo;
import com.caines.cultural.shared.datamodel.UserProfile;
import com.caines.cultural.shared.datamodel.codingscramble.DropMap;
import com.caines.cultural.shared.datamodel.codingscramble.DropMapLevel;
import com.caines.cultural.shared.datamodel.codingscramble.DropMapLevelContainer;
import com.caines.cultural.shared.datamodel.codingscramble.DropMapUser;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class DropMapScramble extends Composite {

	public final static DropScramblerServiceAsync dropService = GWT
			.create(DropScramblerService.class);

	private static CodingScramblePageUiBinder uiBinder = GWT
			.create(CodingScramblePageUiBinder.class);

	interface CodingScramblePageUiBinder extends
			UiBinder<Widget, DropMapScramble> {
	}

	public DropMapScramble() {
		initWidget(uiBinder.createAndBindUi(this));
		if(Window.Location.getParameter("level") != null){
			String first = Window.Location.getParameter("level");
			setupLevel(first);
			return;
		}
		
		if(currentDmlc != null){
			showMap();
			return;
		}
		dropService.getMap(null,new AsyncCallback<Tuple<DropMapLevelContainer,UserProfile>>() {
			
			@Override
			public void onSuccess(Tuple<DropMapLevelContainer,UserProfile> result) {
				currentDmlc = result.a;
				userProfile = result.b;
				if(userProfile.currentMap == -1){
					setupLevel(moveNext());
					return;
				}
				
				
				showMap();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		

//		SimpleFront.scramblerService.getUserInfo(new AsyncCallback<UserInfo>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onSuccess(UserInfo result) {
//				// TODO Auto-generated method stub
//				if (result == null) {
//					profile.setText("Log In");
//
//				} else {
//					profile.setText("Profile");
//				}
//
//			}
//		});

	}

	@UiHandler("profile")
	void onClickProfile(ClickEvent e) {
//		if (profile.getText().equals("Log In")) {
//			Window.Location.assign("/loginRequired");
//		} else {
			ProfileScramble.setupMap();
//		}


	}
	public void addButton(final String b,String type,String label){
		Button a = new Button(label != null? label:b);
		a.getElement().getStyle().setMarginBottom(2, Unit.EM);
		a.setStyleName("btn btn-xl btn-"+type);
		
		a.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				setupLevel(getCurrentLevel().name+"/"+b);
			}
		});
		progression.add(a);
		
	}
	public void showMap() {
		SimpleFront.addKeyHandler(new KeyDownHandler() {

			@Override
			public void onKeyDown(KeyDownEvent event) {

				if (event.getNativeKeyCode() == KeyCodes.KEY_SPACE) {
					setupLevel(getCurrentLevel().name+"/"+getCurrentMap().name);

				}
			}
		});
		for(DropMap dm : getCurrentLevel().maps){
			String type = getMap(dm,getCurrentLevel()).completionStatus;
			
			if(dm.name.equals(getCurrentMap().name)){
				type = "warning";
			}
			if(type == null) continue;
			addButton(dm.name,type,dm.label);
		}
	}
	public static DropMapUser getMap(DropMap dm, DropMapLevel currentLevel) {
		String key = currentLevel.name+"/"+dm.name;
		DropMapUser dropMapUser = userProfile.maps.get(key);
		if(dropMapUser == null){
			dropMapUser = new DropMapUser();
			userProfile.maps.put(key, dropMapUser);
		}
		return dropMapUser;
	}
	@UiField
	VerticalPanel progression;

	@UiField
	Button profile;

	static DropMapLevelContainer currentDmlc;
	static UserProfile userProfile;
	
	public static void setupLevel(String levelName) {
		setCurrentMap(levelName);
		final RootPanel rootPanel = RootPanel.get("content");
		rootPanel.clear();
		CodingScramble cs = new CodingScramble();
		rootPanel.add(cs);
		cs.getCurrentLevel(levelName);
	}

	public static void setupMap() {
		// TODO Auto-generated method stub
		final RootPanel rootPanel = RootPanel.get("content");
		rootPanel.clear();
		rootPanel.add(new DropMapScramble());
	}

	public static void nextMap() {
		
		SimpleFront.removeKeyHandler();
		moveNext();
		
		dropService.updateProfile(userProfile, new FailAlertCallback("Failed to save progress"));
		setupMap();
	}
	
	
	public static String moveNext() {
		if(userProfile.currentMap != -1){
			DropMapUser dropMapUser = getMap(getCurrentMap(), getCurrentLevel());
			if(dropMapUser.completionStatus== null){
				dropMapUser.completionStatus = "info";
			}	
		}
		
		userProfile.currentMap++;
		GWT.log(""+userProfile.maps);
		if (currentDmlc.levels.get(userProfile.currentLevel).maps.size() <= userProfile.currentMap) {
			userProfile.currentLevel++;
			userProfile.currentMap = 0;
			if (userProfile.currentLevel > currentDmlc.levels.size()) {
				userProfile.currentLevel = 0;
			}
		}

		return getRepCurrentLevelMap();
	}

	public static DropMapLevel getCurrentLevel() {
		return currentDmlc.levels.get(userProfile.currentLevel);
	}

	public static DropMap getCurrentMap() {
		return getCurrentLevel().maps.get(userProfile.currentMap);
	}

	public static void setCurrentMap(String levelName) {
		for(int a = 0; a < currentDmlc.levels.size(); a++){
			for(int b = 0; b < currentDmlc.levels.get(a).maps.size(); b++){
				DropMap  dm= currentDmlc.levels.get(a).maps.get(b);
				if((currentDmlc.levels.get(a).name+"/"+dm.name).equals(levelName)){
					userProfile.currentMap = b;
					return;
				}
			}
		}
	}
	public static String getRepCurrentLevelMap(){
		return getCurrentLevel().name+"/"+getCurrentMap().name;
	}

}

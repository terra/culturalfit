package com.caines.cultural.client.ui.codingscramble.drop;

import java.util.ArrayList;
import java.util.List;

import com.caines.cultural.client.SimpleFront;
import com.caines.cultural.shared.datamodel.codingscramble.DropMap;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.media.client.Audio;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class CodingScramble extends Composite {

	public final static DropScramblerServiceAsync dropService = GWT
			.create(DropScramblerService.class);

	private static CodingScramblePageUiBinder uiBinder = GWT
			.create(CodingScramblePageUiBinder.class);

	interface CodingScramblePageUiBinder extends
			UiBinder<Widget, CodingScramble> {
	}

	public CodingScramble() {
		initWidget(uiBinder.createAndBindUi(this));

		// next.getElement().getStyle().setVisibility(Visibility.HIDDEN);

		next.getElement().getStyle().setFontSize(7, Unit.EM);
		SimpleFront.addKeyHandler(new KeyDownHandler() {

			@Override
			public void onKeyDown(KeyDownEvent event) {

				if (event.getNativeKeyCode() == KeyCodes.KEY_SPACE) {

					nextButton();

				}

			}
		});
		next.getElement().setId("next");
		exportStaticMethod();
	}

	@UiField
	Button next;

	Timer timer;

	@UiHandler("next")
	void onClickNext(ClickEvent e) {
		nextButton();
	}

	@UiHandler("back")
	void onClickBack(ClickEvent e) {
		DropMapScramble.setupMap();
	}

	public void getCurrentLevel(String current) {

		DropMap result = DropMapScramble.getCurrentMap();

		Element pBefore = DOM.getElementById("beforeCode");
		pBefore.setInnerHTML(SafeHtmlUtils
				.htmlEscape(joinString(result.debugBefore)));
		execute(joinString(result.debugBefore));
		debugFinish = result.debugFinish;
		debugUpdate = result.debugUpdate;
		// TODO Auto-generated method stub
		if (showLineNumbers) {
			for (String a : result.linesToDrop) {
				showLineNumberCount++;
				linesToDrop.add(showLineNumberCount + ":" + a);
			}
			result.linesToDrop = linesToDrop;
		}
		if (result.order == null) {
			// build one using random number generator
			result.order = new ArrayList<>();
		}
		linesToDrop = new ArrayList<>();
		String[] ordered = new String[result.linesToDrop.size()];
		int ocount = 0;
		for (int a : result.order) {
			ordered[a] = result.linesToDrop.get(ocount);
			ocount++;
		}
		for (int a = ocount; a < result.linesToDrop.size(); a++) {
			ordered[a] = result.linesToDrop.get(a);
		}
		for (String a : ordered)
			linesToDrop.add(a);

		errorLines = result.errorCheck;
		activeLine = linesToDrop.remove(0);

		timer = new Timer() {

			@Override
			public void run() {
				runLine();
			}
		};
		timer.scheduleRepeating(result.speed);
		totalSize = result.size;
		lastEmptySpot = totalSize;
		Element preElement = DOM.getElementById("code");
		preElement.focus();
		Event.sinkEvents(preElement, Event.ONCLICK);

		Event.setEventListener(preElement, new EventListener() {

			@Override
			public void onBrowserEvent(Event event) {
				nextButton();

			}
		});
		runLine();
	}

	List<String> linesToDrop = new ArrayList<>();
	List<String> errorLines = new ArrayList<>();
	List<String> lines = new ArrayList<>();
	List<String> finalLines = new ArrayList<>();
	List<Integer> lineNumbers = new ArrayList<>();
	List<String> debugFinish = new ArrayList<>();
	List<String> debugUpdate = new ArrayList<>();
	int activeLineIndex = 0;
	String activeLine = "";
	int totalSize;
	int lastEmptySpot;
	String lastRun;

	boolean showLineNumbers = false;
	boolean showDebug = true;

	public void runLine() {
		boop.pause();
		boop.getAudioElement().setCurrentTime(0);
		boop.play();
		Element preElement = DOM.getElementById("code");
		if (preElement == null) {
			timer.cancel();
			return;
		}
		StringBuilder c = new StringBuilder();
		activeLineIndex++;
		if (activeLineIndex == lastEmptySpot) {
			lastEmptySpot--;
			if (!setLine()) {
				return;
			}
		}
		finalLines.clear();
		for (int a = 0; a <= totalSize; a++) {
			String lineToAdd = "";
			int count = 0;
			for (int b : lineNumbers) {

				if (a == b) {
					lineToAdd = lines.get(count);
				}
				if (activeLineIndex == b) {
					activeLineIndex++;
				}
				count++;
			}

			if (activeLineIndex == a) {

				lineToAdd = activeLine;
			}
			lineToAdd += "\n";
			finalLines.add(lineToAdd);
			c.append(lineToAdd);
		}
		lastRun = c.toString();
		preElement.setInnerText("");
		preElement.setInnerText(c.toString());

	}

	int showLineNumberCount;

	public boolean setLine() {
		lines.add(activeLine);
		lineNumbers.add(activeLineIndex);
		activeLineIndex = 0;
		tone.pause();
		tone.getAudioElement().setCurrentTime(0);
		tone.play();
		if (linesToDrop.size() == 0) {
			
			timer.cancel();
			GWT.log(lastRun);
			if (showLineNumbers) {
				String a = "";
				for (String b : lastRun.split("\n"))
					a += b.replaceFirst("[0-9:]*", "") + "\n";
				lastRun = a;
			}
			if (showDebug) {
				int speed = 1500;
				try {
					speed = Integer.parseInt(Window.Location
							.getParameter("speed"));
				} catch (NumberFormatException e) {
				}

				executeDebug(lastRun, joinString(debugFinish),
						joinString(debugUpdate), speed,totalSize);
			} else {
				if (!execute(lastRun)) {
					Element preElement = DOM.getElementById("output");
					preElement.setInnerHTML(preElement.getInnerHTML()
							+ "\n Failure, try again");
				} else {
					
					finishSuccessfulRun(joinString(errorLines),
							joinString(debugFinish));
				}
			}
			lines.clear();
			lineNumbers.clear();

			return false;
		}
		
		activeLine = linesToDrop.remove(0);

		return true;
	}

	AsyncCallback<Void> callback = new AsyncCallback<Void>() {

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
			Window.alert("Error");
		}

		@Override
		public void onSuccess(Void result) {
			// TODO Auto-generated method stub

		}
	};

	public static native void executeDebug(String script, String finish,
			String update, int speed,int totalSize) /*-{
		$wnd.runDebugger(script, finish, update, speed,totalSize);
	}-*/;

	public static native void finishSuccessfulRun(String script, String after) /*-{
		parent.finishSuccessfulRun(script, after);
	}-*/;

	public static native boolean execute(String script) /*-{
		try {
			parent.eval(script);
			return true;
		} catch (err) {
			parent.window.document.getElementById("output").innerHTML = err
					.toString();
			return false;
		}
	}-*/;

	public String joinString(List<String> a) {
		StringBuilder sb = new StringBuilder();

		for (String s : a) {
			sb.append(s).append('\n');
		}
		return sb.toString();
	}

	public void nextButton() {
		if ("Click".equals(next.getText())) {
			setLine();

		} else if ("Retry".equals(next.getText())) {
			DropMapScramble.setupLevel(DropMapScramble.getRepCurrentLevelMap());
		} else {
			DropMapScramble.nextMap();
		}

	}
	static private Audio boop;
	static private Audio gong;
	static private Audio hGong;
	static private Audio tone;
	static private Audio tick;

	static {
	    boop =setupSound("boop.wav");
	    gong =setupSound("gong.mp3");
	    hGong =setupSound("happy_gong.wav");
	    tone =setupSound("tone.wav");
	    tick =setupSound("tick.wav");
	}
	public static Audio setupSound(String path){
		Audio sound = Audio.createIfSupported();
		sound.setSrc("assets/sound/"+path);
		return sound;
	}


	public static void finalizeLevel(boolean success) { 
    	if(success)hGong.play();
    	else gong.play();
    }
	public static void playTick() { 
		tick.pause();
		tick.setCurrentTime(0);
		tick.play();
    }
    public static native void exportStaticMethod() /*-{
       $wnd.finalizeLevel =
          $entry(@com.caines.cultural.client.ui.codingscramble.drop.CodingScramble::finalizeLevel(Z));
       $wnd.playTick =
          $entry(@com.caines.cultural.client.ui.codingscramble.drop.CodingScramble::playTick());   
    }-*/;
}



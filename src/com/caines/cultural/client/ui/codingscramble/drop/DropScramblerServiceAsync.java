package com.caines.cultural.client.ui.codingscramble.drop;

import com.caines.cultural.shared.Tuple;
import com.caines.cultural.shared.datamodel.UserProfile;
import com.caines.cultural.shared.datamodel.codingscramble.DropMapLevelContainer;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface DropScramblerServiceAsync {

	void getCurrentLevel(String title, AsyncCallback<ScramblerLevel> callback);



	void getMap(
			String uid,
			AsyncCallback<Tuple<DropMapLevelContainer, UserProfile>> asyncCallback);


	void updateProfile(UserProfile up, AsyncCallback<Void> callback);

}

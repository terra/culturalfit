package com.caines.cultural.client.ui.codingscramble.drop;

import com.caines.cultural.shared.Tuple;
import com.caines.cultural.shared.UserInfo;
import com.caines.cultural.shared.container.ScrambleProfileTransfer;
import com.caines.cultural.shared.container.ScramblerQuestion;
import com.caines.cultural.shared.datamodel.UserProfile;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeLinkContainer;
import com.caines.cultural.shared.datamodel.codingscramble.DropMapLevelContainer;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("drop")
public interface DropScramblerService extends RemoteService {

	ScramblerLevel getCurrentLevel(String title);
	Tuple<DropMapLevelContainer, UserProfile> getMap(String uid);
	
	void updateProfile(UserProfile up);
	
		
	
}

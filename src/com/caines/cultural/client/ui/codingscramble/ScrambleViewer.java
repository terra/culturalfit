package com.caines.cultural.client.ui.codingscramble;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.caines.cultural.shared.Tuple;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainerFile;
import com.caines.cultural.shared.datamodel.codingscramble.CodeLinkContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeLinkContainer.CodeLinkContainerData;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.query.client.Function;
import com.google.gwt.query.client.GQuery;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.objectify.Ref;

public class ScrambleViewer extends Composite  {

	private static ScrambleViewerUiBinder uiBinder = GWT
			.create(ScrambleViewerUiBinder.class);

	interface ScrambleViewerUiBinder extends UiBinder<Widget, ScrambleViewer> {
	}

	public ScrambleViewer() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	Anchor toSubmit;
	@UiField
	Anchor currentTag;

	@UiField
	VerticalPanel crossLink;

	public ScrambleViewer(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
		
		init();
	}

	public ScrambleViewer init() {
		String[] u = Window.Location.getHref().split("/");
	
		ScrambleFrontPage.basicService.getContainer(u[4], new AsyncCallback<Tuple<CodeContainer,CodeLinkContainer>>() {
			
			@Override
			public void onSuccess(Tuple<CodeContainer, CodeLinkContainer> result) {
				// TODO Auto-generated method stub
				setupCode(result.a, "code", result.b);
				for(int a = 0; a < result.b.linkedContainerNames.size();a++){
					String b = result.b.linkedContainerNames.get(a);
					String name = b.substring(b.lastIndexOf('/'));
					crossLink.add(new Anchor(name, "/viewer/"+result.b.linkedContainerIds.get(a)));
					crossLink.add(new Label(result.b.linkedKeyWords.get(a).toString()));
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		return this;
	}

//	@UiHandler("button")
//	void onClick(ClickEvent e) {
//		Window.alert("Hello!");
//	}

	
	Map<String,Integer> nameColorMap = new HashMap<String, Integer>(); 
	public void setupCode(CodeContainer code, String name, CodeLinkContainer toHighlight) {
		Element preElement = DOM.getElementById(name);
		preElement.setInnerText("");
		CodeContainerFile ccf = code.ccf;
		boolean isComment = false;
		for(CodeLinkContainerData tH : toHighlight.clcList){
			Integer color =  nameColorMap.get(tH.name);
			if(color == null){
				count++;
				if(count > maxCount) count = 1;
				color = count;
				nameColorMap.put(tH.name, color);
				System.out.println(tH.name);
			}
//			if("span".contains(tH.name)){
//				continue;
//			}
			
			for(Integer i : tH.linkedPointers){				
				ccf.file.set(i, ccf.file.get(i).replaceAll("([^A-Za-z0-9_-])"+tH.name+"([^A-Za-z0-9_-])", updateName(tH, color)));
			}
			
			for(Integer i : tH.notLinkedPointers){				
				ccf.file.set(i, ccf.file.get(i).replaceAll("([^A-Za-z0-9_-])"+tH.name+"([^A-Za-z0-9_-])",updateName(tH, color)));
			}
		}
		preElement.setInnerHTML(ccf.getRawFile());
		preElement.removeClassName("prettyprinted");
		ScrambleFrontPage.runPretty();
		GQuery myElement = GQuery.$(".linkedName");
		
		myElement.click(new Function() {
			@Override
			public boolean f(Event e) {
				final String clickedName = GQuery.$(e).text();
				handlePageClick(clickedName);
				
				return false;
			}
			
		});
		
		GQuery pageElement = GQuery.$("#"+name);
		pageElement.click(new Function() {
			@Override
			public boolean f(Event e) {
				
				handlePageClick(null);
				
				return false;
			}
			
		});
	}

	public String updateName(CodeLinkContainerData tH, Integer color) {
		return "$1<span class='link-color-"+color+" linkedName' >"+tH.name+"</span>$2";
	}
	public void handlePageClick(final String clickedName) {
		GQuery.$(".linkedName").each(new Function(){
			@Override
			public Object f(Element element, int i) {
				if(clickedName != null&&!clickedName.equals(GQuery.$(element).text())){								
					element.setClassName(element.getClassName().replace("color", "unused1"));	
				} else {
					element.setClassName(element.getClassName().replace("unused1", "color"));
				}
				
				//GQuery.$(element).unbind("click");
				return null;
			}
		});
	}

	int count = 0;
	final int maxCount = 11;
	
	public static native void runPretty() /*-{
	$wnd.prettyPrint();
}-*/;
}

package com.caines.cultural.client.ui.codingscramble;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.caines.cultural.client.SimpleFront;
import com.caines.cultural.shared.container.ScrambleProfileTransfer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeUserDetails;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.PreElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.SuggestOracle.Callback;
import com.google.gwt.user.client.ui.SuggestOracle.Request;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

public class ScrambleTagSearch extends Composite {

	private static ScrambleProfileUiBinder uiBinder = GWT
			.create(ScrambleProfileUiBinder.class);

	interface ScrambleProfileUiBinder extends
			UiBinder<Widget, ScrambleTagSearch> {
	}

	@UiField
	VerticalPanel content;

	@UiField(provided = true)
	// MAKE SURE YOU HAVE THIS LINE
	SuggestBox suggestBox;

	public ScrambleTagSearch() {
		MultiWordSuggestOracle oracle = new MultiWordSuggestOracle();
		suggestBox = new SuggestBox(oracle);
		oracle.add("python");
		oracle.add("java");
		oracle.add("C++");
		oracle.add("C");
		initWidget(uiBinder.createAndBindUi(this));
		suggestBox.addSelectionHandler(new SelectionHandler<SuggestOracle.Suggestion>() {
			
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				Window.Location.assign(Window.Location.createUrlBuilder().removeParameter("searchTags").setParameter("tag", event.getSelectedItem().getReplacementString()).buildString());
			}
		});
	    Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
	        public void execute () {
	        	suggestBox.setFocus(true);
	        	//Window.alert("aoeu");
	        }
	    });
		
	}
	

	// @UiHandler("button")
	// void onClick(ClickEvent e) {
	// Window.alert("Hello!");
	// }

}

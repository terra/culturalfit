package com.caines.cultural.client.ui.codingscramble;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.caines.cultural.client.SimpleFront;
import com.caines.cultural.shared.container.ScrambleProfileTransfer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeUserDetails;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.PreElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class ScrambleProfile extends Composite {

	private static ScrambleProfileUiBinder uiBinder = GWT
			.create(ScrambleProfileUiBinder.class);

	interface ScrambleProfileUiBinder extends UiBinder<Widget, ScrambleProfile> {
	}

	public ScrambleProfile() {
		initWidget(uiBinder.createAndBindUi(this));

		SimpleFront.scramblerService
				.getProfileContent(new AsyncCallback<ScrambleProfileTransfer>() {
					public void onSuccess(ScrambleProfileTransfer result) {
						PreElement pe = Document.get().createPreElement();
						profileContent.getElement().appendChild(pe);
						String text = "";
						for (Entry<String, Integer> e : result.promotedCount
								.entrySet()) {
							text += "\n\tViewed promoted code for " + e.getKey() + " "
									+ e.getValue() + " times";
						}
						text +="\n\n\n";
						for (Entry<String, Double> e : result.promotedCorrectness
								.entrySet()) {
							text += "\n\tCorrectness ratio of promoted code for " + e.getKey() + " "
									+ e.getValue() + " ";
						}
						text +="\n\n\n";
						for (Entry<String, Integer> e : result.countSection
								.entrySet()) {
							text += "\n\tViewed code for " + e.getKey() + " "
									+ e.getValue() + " times";
						}
						pe.setInnerText(text);

					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						caught.printStackTrace();
					}
				});
	}

	@UiField
	VerticalPanel profileContent;

	// @UiHandler("button")
	// void onClick(ClickEvent e) {
	// Window.alert("Hello!");
	// }

}

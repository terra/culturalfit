package com.caines.cultural.client;

import java.util.List;
import java.util.Map;

import com.caines.cultural.shared.Tuple;
import com.caines.cultural.shared.UserInfo;
import com.caines.cultural.shared.container.ScrambleProfileTransfer;
import com.caines.cultural.shared.container.ScramblerQuestion;
import com.caines.cultural.shared.datamodel.codingscramble.CodeContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeLinkContainer;
import com.caines.cultural.shared.datamodel.codingscramble.CodeUserDetails;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface BasicScramblerServiceAsync {

	void addCodePage(String url, String tags, AsyncCallback<Void> callback);

	void getProfileContent(AsyncCallback<ScrambleProfileTransfer> asyncCallback);

	void getUserInfo(AsyncCallback<UserInfo> callback);

	void getNextLines(String tag, boolean showPromote,
			AsyncCallback<ScramblerQuestion> callback);

	void linkCode(String linkType, AsyncCallback<Void> callback);

	void getContainer(String associatedUrl,
			AsyncCallback<Tuple<CodeContainer, CodeLinkContainer>> callback);

	void promote(AsyncCallback<Void> callback);


}

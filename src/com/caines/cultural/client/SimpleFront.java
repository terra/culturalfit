package com.caines.cultural.client;

import com.caines.cultural.client.ui.codingscramble.ScrambleFrontPage;
import com.caines.cultural.client.ui.codingscramble.ScrambleProfile;
import com.caines.cultural.client.ui.codingscramble.ScrambleTagSearch;
import com.caines.cultural.client.ui.codingscramble.ScrambleViewer;
import com.caines.cultural.client.ui.codingscramble.ScramblerSubmit;
import com.caines.cultural.client.ui.codingscramble.drop.CodingScramble;
import com.caines.cultural.client.ui.codingscramble.drop.DropMapScramble;
import com.caines.cultural.client.ui.codingscramble.drop.ProfileScramble;
import com.caines.cultural.shared.UserInfo;
import com.caines.cultural.shared.datamodel.GUser;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.GWT.UncaughtExceptionHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class SimpleFront implements EntryPoint {

	/**
	 * Create a remote service proxy to talk to the server-side Greeting
	 * service.
	 */
	public final static BasicScramblerServiceAsync scramblerService = GWT
			.create(BasicScramblerService.class);

	public static SimpleFront singleton;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		GWT.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			
			@Override
			public void onUncaughtException(Throwable e) {
				// TODO Auto-generated method stub
				//e.printStackTrace();
				GWT.log("what", e);
			}
		});
		DropMapScramble.setupMap();
		if(true)return;
		String[] u = Window.Location.getHref().split("/");
		
		if(u.length > 3){
			if ((u[3].startsWith("profile"))) {
				ProfileScramble.setupMap();
				return;
			}
			if ((u[3].startsWith("drop"))) {
				DropMapScramble.setupMap();
				return;
			}
			if ("viewer".equalsIgnoreCase(u[3])) {
				setupViewerPage();
				return;
			}
			if ("true".equals(Window.Location.getParameter("searchTags"))) {
				SimpleFront.setupTopOfSidePage().add(new ScrambleTagSearch());
				return;
			}	
		}
		

		setupFrontPage();
	}

	public static void setupViewerPage() {
		final RootPanel rootPanel = RootPanel.get("content");
		ScrambleViewer scrambleViewer = new ScrambleViewer();
		scrambleViewer.init();
		rootPanel.add(scrambleViewer);

	}

	public static void setupFrontPage() {
		final RootPanel rootPanel = RootPanel.get("content");
		rootPanel.add(new ScrambleFrontPage());

	}

	public static RootPanel setupTopOfSidePage() {
		final RootPanel rootPanel = RootPanel.get("content");
		rootPanel.clear();
		Button button = new Button("To Frontpage");
		button.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				rootPanel.clear();
				setupFrontPage();
			}
		});
		rootPanel.add(button);
		return rootPanel;
	}

	public static void addUrl() {
		RootPanel.get("content");
		final RootPanel rootPanel = setupTopOfSidePage();
		rootPanel.add(new ScramblerSubmit());
	}

	static HandlerRegistration addDomHandler;

	public static void addKeyHandler(KeyDownHandler kdh) {
		if (addDomHandler != null)
			addDomHandler.removeHandler();
		addDomHandler = RootPanel.get().addDomHandler(kdh,
				KeyDownEvent.getType());

	}
	public static void removeKeyHandler() {
		if (addDomHandler != null)
			addDomHandler.removeHandler();

	}
}

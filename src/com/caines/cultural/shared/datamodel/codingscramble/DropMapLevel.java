package com.caines.cultural.shared.datamodel.codingscramble;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DropMapLevel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public List<DropMap> maps = new ArrayList<>();
	public String name;
	public DropMapLevel() {
		// TODO Auto-generated constructor stub
	}
	public DropMapLevel(String name) {
		super();
		this.name = name;
	}
}

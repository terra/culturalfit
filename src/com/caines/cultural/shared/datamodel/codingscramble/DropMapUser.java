package com.caines.cultural.shared.datamodel.codingscramble;

import java.io.Serializable;


public class DropMapUser implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DropMapUser() {
	
	}
	public String completionStatus;

	@Override
	public String toString() {
		return "DropMapUser [completionStatus=" + completionStatus + "]";
	}
	

}

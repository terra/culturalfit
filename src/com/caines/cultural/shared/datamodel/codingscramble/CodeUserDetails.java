package com.caines.cultural.shared.datamodel.codingscramble;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.caines.cultural.shared.datamodel.GUser;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class CodeUserDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CodeUserDetails() {

	}

	@Id
	public Long id;

	
	
	@Index
	public Ref<GUser> gUser;
	public Map<String,Integer> countMap = new HashMap<String, Integer>();
	public Map<String,Integer> promotedCount = new HashMap<>();
	public Map<String,Integer> promotedCorrect = new HashMap<>();
}

package com.caines.cultural.shared.datamodel.codingscramble;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class DropMap implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public DropMap() {
		// TODO Auto-generated constructor stub
	}
	public int size = 10;
	
	public int speed;
	public String name;
	public String label;
	public DropMap(String name) {
		super();
		this.name = name;
	}
	public DropMap(String name, String label,int speed) {
		super();
		this.name = name;
		this.label = label;
		this.speed = speed;
	}
	public DropMap(String name, String label) {
		this(name,label,500);
	}


	public List<String> linesToDrop = new ArrayList<>();
	public List<String> errorCheck = new ArrayList<>();
	public List<Integer> order;
	public List<String> debugUpdate= new ArrayList<>();
	public List<String> debugFinish = new ArrayList<>();
	public List<String> debugBefore = new ArrayList<>();

}

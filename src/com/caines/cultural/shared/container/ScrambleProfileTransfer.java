package com.caines.cultural.shared.container;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.safehtml.shared.SafeUri;

public class ScrambleProfileTransfer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Map<String,Integer> countSection;
	public Map<String,Integer> promotedCount;
	public Map<String,Double> promotedCorrectness = new HashMap<String, Double>();
	
}
